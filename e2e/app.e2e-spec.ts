import { AniDslPage } from './app.po';

describe('ani-dsl App', function() {
  let page: AniDslPage;

  beforeEach(() => {
    page = new AniDslPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
