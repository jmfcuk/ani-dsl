import { Component, OnInit } from '@angular/core';
import { PAGE_ANIMATIONS } from '../shared/page-animations';
import { AniDslTestComponent } from '../ani-dsl-test/ani-dsl-test.component';

@Component({
  moduleId: module.id,
  selector: 'app-page1',
  templateUrl: 'page1.component.html',
  styleUrls: ['page1.component.css'],
  directives: [ AniDslTestComponent ],
  animations: [ PAGE_ANIMATIONS ]
})
export class Page1Component implements OnInit {

  imgUrl0: string = 'https://cdn2.f-cdn.com/contestentries/329981/17879915/569a370ee2c15_thumb900.jpg';

  imgUrl1: string = 'http://www.espacoaquarius.com.br/Fotos/workshop_julho2015.jpg';

  imgUrl2: string = 'http://im.ziffdavisinternational.com/ign_pl/tech/n/nasa/nasa_nm8a.jpg';

  constructor() { }

  ngOnInit() {
  }
}
