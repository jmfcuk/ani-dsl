import { Component, OnInit } from '@angular/core';
import { PAGE_ANIMATIONS } from '../shared/page-animations';
import { Page2SubComponent } from './page2-sub/page2-sub.component';

@Component({
  moduleId: module.id,
  selector: 'app-page2',
  templateUrl: 'page2.component.html',
  styleUrls: ['page2.component.css'],
  animations: [ PAGE_ANIMATIONS ],
  directives: [ Page2SubComponent ]
})
export class Page2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
