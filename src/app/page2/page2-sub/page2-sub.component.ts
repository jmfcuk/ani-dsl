import { Component, OnInit, AUTO_STYLE, trigger, state, animate, transition, style } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-page2-sub',
  templateUrl: 'page2-sub.component.html',
  styleUrls: ['page2-sub.component.css'],

  animations: [

    trigger('show', [

        transition('void => *', [
            style({ opacity: '0' }),
            animate('8000ms linear')
        ]),

        // https://github.com/angular/angular/issues/9350
        ,
        transition('* => void', [
            animate('8000ms linear', style({ opacity: '0' })),
        ])
    ]),

    trigger('select', [
      state('selected', style({ background: 'yellow', border: ' 2px solid red' })),
      state('unselected', style({ background: AUTO_STYLE, border: '1px solid black' }))
    ])
  ]
})
export class Page2SubComponent implements OnInit {

  selected: string;

  constructor() { this.selected = 'unselected'; }

  ngOnInit(): void {
  }

  toggleSelected() : void {
    if(this.selected === 'selected')
      this.selected = 'unselected';
    else
      this.selected = 'selected';
  }
}
