/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { Page2Component } from './page2.component';

describe('Component: Page2', () => {
  it('should create an instance', () => {
    let component = new Page2Component();
    expect(component).toBeTruthy();
  });
});
