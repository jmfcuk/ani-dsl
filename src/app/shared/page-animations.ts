import { AUTO_STYLE, trigger, state, animate, transition, style } from '@angular/core';

export let PAGE_ANIMATIONS =
    trigger('pageAnimation', [
        transition('void => *', [
            style({ width: '0px', height: AUTO_STYLE }),
            animate('1000ms linear')
        ]),

        // https://github.com/angular/angular/issues/9350
        ,
        transition('* => void', [
            animate('1000ms linear', style({ width: '0px', height: '0px' })),
        ])
    ])
