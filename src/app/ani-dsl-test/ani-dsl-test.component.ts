import { Component, Input,
         AUTO_STYLE, trigger, state, animate, transition, style,
         OnInit, OnDestroy } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-ani-dsl-test',
  templateUrl: 'ani-dsl-test.component.html',
  styleUrls: ['ani-dsl-test.component.css'],

  animations: [
    trigger('openCloseImg', [
      state('closed, void',
        style({ width: '0px', height: '0px' })),
      state('open',
        style({ width: AUTO_STYLE, height: AUTO_STYLE })),
      transition('closed <=> open', [
        animate('1000ms linear')
      ])
    ]),

    trigger('hoverImg', [
      state('hover',
        style({ transform: 'scale(1.4) rotateY(360deg)' })),
        transition('* => hover', [
          animate('1000ms linear')
        ]),
      state('nohover',
        style({ width: AUTO_STYLE, height: AUTO_STYLE })),
      transition('hover <=> nohover', [
        animate('1000ms linear')
      ])
    ]),
  ]
})
export class AniDslTestComponent implements OnInit {

  private _OPEN_IMG_PROMPT: string = 'Open';
  private _CLOSE_IMG_PROMPT: string = "Close"
  
  private _IMG_OPENED_STATE = "open";
  private _IMG_CLOSED_STATE = "closed";

  prompt: string;

  openCloseImgState: string;
  
  hoverImgState: string;

  @Input()
  imgUrl: string;// = 'http://weknowmemes.com/wp-content/uploads/2013/09/wat-wat-wat-meme.jpg';

  constructor() {
    this.hideImg();
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }

  showImg(): void {
    this.openCloseImgState = this._IMG_OPENED_STATE;
    this.prompt = this._CLOSE_IMG_PROMPT;
  }

  hideImg(): void {
    this.openCloseImgState = this._IMG_CLOSED_STATE;
    this.prompt = this._OPEN_IMG_PROMPT;
  }

  toggleImg(): void {
    if (this._IMG_OPENED_STATE === this.openCloseImgState)
      this.hideImg();
    else
      this.showImg();
  }

  hoverImg(hover: boolean): void {
    if(hover)
      this.hoverImgState = 'hover';
      else
      this.hoverImgState = 'nohover';
  }
}
