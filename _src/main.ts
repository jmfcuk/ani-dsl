import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppComponent, environment } from './app/';
import { APP_ROUTES } from './app/app-route-config';

if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent,  [ APP_ROUTES ]);
