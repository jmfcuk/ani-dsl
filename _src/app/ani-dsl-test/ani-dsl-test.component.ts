import { Component, Input,
         AUTO_STYLE, trigger, state, animate, transition, style,
         OnInit, OnDestroy } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-ani-dsl-test',
  templateUrl: 'ani-dsl-test.component.html',
  styleUrls: ['ani-dsl-test.component.css'],

  animations: [
    trigger('imgOpenClose', [
      state('closed, void',
        style({ width: '0px', height: '0px' })),
      state('open',
        style({ width: AUTO_STYLE, height: AUTO_STYLE })),
      transition('closed <=> open', [
        animate(1000)
      ])
    ]),

    trigger('imgHover', [
      state('hover',
        style({ transform: 'scale(1.4) rotateY(360deg)' })),
        transition('* => hover', [
          animate(1000)
        ]),
      state('nohover',
        style({ width: AUTO_STYLE, height: AUTO_STYLE, transform: 'rotateY(-360deg)' })),
      transition('hover <=> nohover', [
        animate(1000)
      ])
    ]),
  ]
})
export class AniDslTestComponent implements OnInit {

  private _OPEN_IMG_PROMPT: string = 'Open';
  private _CLOSE_IMG_PROMPT: string = "Close"
  
  private _IMG_OPENED_STATE = "open";
  private _IMG_CLOSED_STATE = "closed";

  prompt: string;

  imgOpenCloseState: string;
  
  imgHoverState: string;

  @Input()
  imgUrl: string;// = 'http://weknowmemes.com/wp-content/uploads/2013/09/wat-wat-wat-meme.jpg';

  constructor() {
    this.hideImg();
    this.imgHoverState = 'nohover';
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }

  showImg(): void {
    this.imgOpenCloseState = this._IMG_OPENED_STATE;
    this.prompt = this._CLOSE_IMG_PROMPT;
  }

  hideImg(): void {
    this.imgOpenCloseState = this._IMG_CLOSED_STATE;
    this.prompt = this._OPEN_IMG_PROMPT;
  }

  toggleImg(): void {
    if (this._IMG_OPENED_STATE === this.imgOpenCloseState)
      this.hideImg();
    else
      this.showImg();
  }

  hoverImg(hover: boolean): void {
    if(hover)
      this.imgHoverState = 'hover';
    else
      this.imgHoverState = 'nohover';
  }
}
