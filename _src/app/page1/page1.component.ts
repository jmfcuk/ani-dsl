import { Component, OnInit } from '@angular/core';
import { PAGE_ANIMATIONS } from '../shared/page-animations';
import { AniDslTestComponent } from '../ani-dsl-test/ani-dsl-test.component';

@Component({
  moduleId: module.id,
  selector: 'app-page1',
  templateUrl: 'page1.component.html',
  styleUrls: ['page1.component.css'],
  directives: [ AniDslTestComponent ],
  animations: [ PAGE_ANIMATIONS ]
})
export class Page1Component implements OnInit {

  imgUrl: string = 'http://weknowmemes.com/wp-content/uploads/2013/09/wat-wat-wat-meme.jpg';

  constructor() { }

  ngOnInit() {
  }
}
