/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { Page1Component } from './page1.component';

describe('Component: Page1', () => {
  it('should create an instance', () => {
    let component = new Page1Component();
    expect(component).toBeTruthy();
  });
});
