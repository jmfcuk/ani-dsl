/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { Page2SubComponent } from './page2-sub.component';

describe('Component: Page2Sub', () => {
  it('should create an instance', () => {
    let component = new Page2SubComponent();
    expect(component).toBeTruthy();
  });
});
