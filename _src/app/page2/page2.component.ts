import { Component, OnInit } from '@angular/core';
import { PAGE_ANIMATIONS } from '../shared/page-animations';

@Component({
  moduleId: module.id,
  selector: 'app-page2',
  templateUrl: 'page2.component.html',
  styleUrls: ['page2.component.css'],
  animations: [ PAGE_ANIMATIONS ]
})
export class Page2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
